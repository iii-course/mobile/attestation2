package com.example.task2.utils;

import android.graphics.Bitmap;

public class BitmapHelpers {
    public static Bitmap scaleImage(Bitmap original, int width, int height) {
        return Bitmap.createScaledBitmap(original, width, height, false);
    }
 }
