package com.example.task1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class AnimationView extends View {
    private int k = 3;
    private float R, r;
    private float circleCentreX, circleCentreY;
    private int currentAngle = 0;
    private int angleStep = 1;

    private float paintWidth = 5;

    private int circleColor = Color.LTGRAY;
    private int lineColor = Color.BLUE;

    public AnimationView(Context context) {
        super(context);
    }

    public AnimationView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AnimationView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setCoefficient(int k) {
        this.k = k;
        r = R / k;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawCircle(canvas);

        currentAngle += angleStep;
        if (currentAngle >= 480) {
            currentAngle = 0;
        }

        for (int i = 0; i < currentAngle; i += 1) {
            drawPointAtAngle(i, canvas);
        }

        invalidate();
    }

    private void setUpCircleCharacteristics(Canvas canvas) {
        circleCentreX = canvas.getWidth() / 2;
        circleCentreY = canvas.getHeight() / 2;
        R = canvas.getWidth() / 4;
        r = R / k;
    }

    private void drawCircle(Canvas canvas) {
        setUpCircleCharacteristics(canvas);
        Paint paint = getPaint(circleColor);
        canvas.drawPoint(circleCentreX, circleCentreY, paint);
        canvas.drawCircle(circleCentreX, circleCentreY, R, paint);
    }

    private void drawPointAtAngle(int angle, Canvas canvas) {
        float radian = getRadian(angle);
        float radianWithCoefficient = getRadian((k + 1) * angle);
        float x = circleCentreX +
                (float) (r * (k + 1) * (Math.cos(radian) - Math.cos(radianWithCoefficient) / (k + 1)));
        float y = circleCentreY +
                (float) (r * (k + 1) * (Math.sin(radian) - Math.sin(radianWithCoefficient) / (k + 1)));

        canvas.drawPoint(x, y, getPaint(lineColor));
    }

    private Paint getPaint(int color) {
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(paintWidth);
        return paint;
    }

    private float getRadian(float angle) {
        return (float) (angle * Math.PI / 180);
    }
}
