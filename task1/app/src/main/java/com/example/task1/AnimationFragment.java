package com.example.task1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class AnimationFragment extends Fragment {
    private FloatingActionButton upButton, downButton;
    private AnimationView view;

    private int currentCoefficient = 3;
    private final int step = 1;

    private int maxCoefficient = 10;
    private int minCoefficient = 1;

    public AnimationFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_animation, container, false);

        upButton = rootView.findViewById(R.id.upButton);
        downButton = rootView.findViewById(R.id.downButton);
        view = rootView.findViewById(R.id.animation5View);

        upButton.setOnClickListener(this::increaseCoefficient);
        downButton.setOnClickListener(this::decreaseCoefficient);

        view.setCoefficient(currentCoefficient);

        return rootView;
    }

    private void decreaseCoefficient(View view) {
        if (currentCoefficient - step < maxCoefficient && currentCoefficient - step >= minCoefficient) {
            currentCoefficient -= step;
        }
        this.view.setCoefficient(currentCoefficient);
    }

    private void increaseCoefficient(View view) {
        if (currentCoefficient + step <= maxCoefficient && currentCoefficient + step > minCoefficient) {
            currentCoefficient += step;
        }
        this.view.setCoefficient(currentCoefficient);
    }
}
